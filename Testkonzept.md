# Testkonzept für die BizTrips-App

**1.. Einleitung:**
   1. Ziel des Tests
      - Gewährleistung der Funktionalität und Zuverlässigkeit der BizTrips-App.
   2. Umfang des Tests
      - Abdeckung von grundlegenden Funktionen wie das Hinzufügen von Elementen zur Wishlist, das Löschen von Elementen aus der Wishlist, und die Leerung der Wishlist.

**2. Testumgebung:**
   1. Hardware
      - Desktop (Windows)
   2. Software
      - Betriebssysteme: Windows 1.1.
      - Browser: Microsoft Edge
      - Testumgebung für Entwickler: Visual Studio Code
      - Testautomatisierungstools: Postman
      
**3. Testziele:**
   1. Gewährleistung der grundlegenden Funktionalitäten:
      - Hinzufügen von Produkten zur Wishlist
      - Löschen von Produkten aus der Wishlist
      - Leerung der Wishlist
      - Filtern der Trips nach Monat
   2. Sicherstellung der Benutzerfreundlichkeit:
      - Intuitives Benutzererlebnis beim Hinzufügen und Bearbeiten von Einträgen
   3. Überprüfung der Plattformkompatibilität:
      - Funktionalität auf Windows-Geräten

**4. Testphasen:**
   1. Unit Testing
      - Überprüfung der einzelnen Module auf korrekte Funktionalität
   2. Integration Testing
      - Gewährleistung der reibungslosen Interaktion zwischen den verschiedenen Modulen
   3. System Testing
      - Testen der Gesamtfunktionalität der BizTrips-App

**5. Testfälle:**
   1. Hinzufügen eines Trips zur Wishlist
   2. Löschen eines Produkts aus der Wishlist
   3. Leerung der Wishlist
   4. Überprüfung der Benutzeroberfläche auf Konsistenz und Benutzerfreundlichkeit

**6. Testautomatisierung:**
   1. Automatisierung der wiederholbaren Testfälle
   2. Einsatz von Postman für automatisiertes Testen der API
   3. Integration von Testautomatisierung in den Continuous Integration-Prozess

**7. Risikomanagement:**
   1. Identifikation potenzieller Risiken wie Performance-Probleme, Datenverlust oder Inkompatibilität mit bestimmten Geräten
   2. Implementierung von Maßnahmen zur Risikoreduzierung

**8. Akzeptanzkriterien:**
   1. Die App funktioniert fehlerfrei auf allen unterstützten Plattformen
   2. Alle grundlegenden Funktionen können ohne Probleme durchgeführt werden
   3. Die Benutzeroberfläche entspricht den Designrichtlinien und bietet eine positive Benutzererfahrung

**9. Testbericht:**
   1. Dokumentation der Testergebnisse
   2. Auflistung gefundener Fehler und deren Status
   3. Empfehlungen für die Freigabe der App